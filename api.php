<?php
/**
 * Created by PhpStorm.
 * User: neboula
 * Date: 2019-03-19
 * Time: 12:43
 */

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
require_once 'dbm.php';

$data = [];

if (isset($_GET['startDay'])) {
    $currentTime = time();
    $startTime = strtotime('-' . $_GET['startDay'] . ' day', $currentTime);
    $endTime = strtotime('-' . $_GET['endDay'] . ' day', $currentTime);
}

//$currentTime = time();

//$offsetTime = strtotime('-14 day', $currentTime);

$query = 'SELECT * FROM weather_data WHERE time BETWEEN ' . $startTime . ' AND ' . $endTime . ';';

$stm = $dbm->prepare($query);
$stm->execute();

echo json_encode($stm->fetchAll());
