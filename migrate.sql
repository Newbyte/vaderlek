USE weather;

DROP TABLE IF EXISTS weather_data;

CREATE TABLE weather_data
(
  id                              INT UNSIGNED AUTO_INCREMENT,
  time                            INTEGER,
  interval_                       TINYINT UNSIGNED,
  temp_indoors                    DECIMAL(4, 1),
  humidity_indoors                TINYINT UNSIGNED,
  temp_outdoors                   DECIMAL(4, 1),
  humidity_outdoors               TINYINT UNSIGNED,
  relative_atmospheric_pressure   DECIMAL(6, 1),
  absolute_atmospheric_pressure   DECIMAL(6, 1),
  wind_speed                      DECIMAL(6, 1),
  squall                          DECIMAL(6, 1),
  wind_direction                  VARCHAR(3),
  dew_point                       DECIMAL(4, 1),
  wind_chill                      DECIMAL(6, 1),
  rainfall_1h                     DECIMAL(6, 1),
  rainfall_24h                    DECIMAL(6, 1),
  rainfall_week                   DECIMAL(8, 1),
  rainfall_month                  DECIMAL(10, 1),
  rainfall_total                  DECIMAL(12, 1),
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8
