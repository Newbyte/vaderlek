# Väderlek
## Mjukvara som krävs för att köra
- PHP 7
  - php
  - php-mysqlnd
  - php-mbstring
  - php-pecl-json
- MariaDB (rekommenderas) eller MySQL
## Filer
### seed-cli.php
#### Beskrivning
Används för att importera data från en specificerad csv-fil till MySQL-databasen. Bör köras via CLI.
#### Syntax
`php seed-cli.php [filnamn]`

Filnamn är valbart. Om inte specificerat används `STDOUT` samt `STDIN`.
### api.php
#### Beskrivning
Skickar tillbaka data från databasen i JSON format. Tar parametrarna startDay och endDay via GET. 
### dbm.php
#### Beskrivning
Innehåller inställningar för anslutning till MariaDB/MySQL samt hjälp-funktioner för arbete med dessa.