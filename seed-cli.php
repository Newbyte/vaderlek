<?php
/**
 * Created by PhpStorm.
 * User: neboula
 * Date: 2019-03-15
 * Time: 15:21
 */

require_once 'dbm.php';

$tableName = 'weather_data';
$defaultFilename = 'weather.csv';

function getLine(): string
{
    return trim(fgets(STDIN));
}

function getFileStream(string $filename)
{
    return fopen($filename, 'r');
}

function insertData(PDO $dbm, $file, string $filename): void
{
    $i = 0;
    while (($data = fgetcsv(($file), filesize($filename), "\n")) !== false) {
        if ($i !== 0) {
            $convertedData = mb_convert_encoding($data[0], 'UTF-8', 'UTF-16');
            $convertedData = explode(' ', $convertedData);

            // Remove unnecessary elements
            $convertedData = array_splice($convertedData, 2, count($convertedData) - 2);

            foreach ($convertedData as &$convertedDatum) {
                $convertedDatum = trim($convertedDatum);
            }

            if (count($convertedData) !== 0) {
                if (strlen($convertedData[0]) !== 0) {
                    $timestamp = strtotime($convertedData[0] . ' ' . $convertedData[1]);
                    $query = 'INSERT INTO weather_data VALUES (NULL, "' . $timestamp . '", "' . $convertedData[2] . '", "' . $convertedData[3] . '", "' . $convertedData[4] . '", "' . $convertedData[5] . '", "' . $convertedData[6] . '", "' . $convertedData[7] . '", "' . $convertedData[8] . '", "' . $convertedData [9] . '", "' . $convertedData[10] . '", "' . $convertedData[11] . '", "' . $convertedData[12] . '", "' . $convertedData[13] . '", "' . $convertedData[14] . '", "' . $convertedData[15] . '", "' . $convertedData[16] . '", "' . $convertedData[17] . '", "' . $convertedData[18] . '");';
                }
            }

            execQuery($dbm, $query);
        }
        $i++;
    }
}

if ($argc > 1) {
    $filename = $argv[1];

    $file = getFileStream($filename);
    clearTable($dbm, $tableName);
    insertData($dbm, $file, $filename);
} else {
    echo 'VARNING: Detta kommer att ta bort all data i tabellen "' . $tableName . '". Är du säker på att du vill fortsätta? (j/N)' . "\n";
    echo 'Input: ';

    $line = getLine();

    if ($line !== 'j') {
        echo 'Sunt svar, avbryter ...' . "\n";
        exit;
    }

    echo 'Namn på fil att hämta data ifrån (' . $defaultFilename . ' används om detta lämnas tomt): ';
    $filename = getLine();

    if (strlen($filename) === 0) {
        $filename = $defaultFilename;
    }

    $file = getFileStream($filename);

    if ($file === false) {
        echo 'Filen "' . $filename . '" kunde inte hittas. Avbryter.' . "\n";
        exit;
    }

    echo 'Tömmer tabell "' . $tableName . '" ...' . "\n";
    clearTable($dbm, $tableName);
    echo '... tabell tömd' . "\n";

    echo 'Sparar data från fil "' . $filename . '" i tabell "' . $tableName . '" ...' . "\n";
    insertData($dbm, $file, $filename);
    echo '... data sparad i databas' . "\n";
}

