<?php
/**
 * Created by PhpStorm.
 * User: neboula
 * Date: 2019-03-12
 * Time: 13:46
 */

$host = 'localhost';
$user = 'root';
$pass = '';
$db   = 'weather';

$dsn = 'mysql:dbname=' . $db . ';host=' . $host . ';charset=utf8';

$settings = array(
    \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
);

try {
    $dbm = new \PDO($dsn, $user, $pass, $settings);
} catch (PDOException $exception) {
    echo 'Failed to connect to db :( Reason: ' . $exception->getMessage();
    exit;
}

function execQuery(PDO $dbm, string $query): void {
    try {
        $stm = $dbm->prepare($query);
        $stm->execute();
    } catch (PDOException $exception) {
        echo 'Something went wrong: ' . $exception->getMessage() . "\n";
    }
}

function clearTable(PDO $dbm, string $tableName): void {
    execQuery($dbm, 'DELETE FROM ' . $tableName . '; ALTER TABLE ' . $tableName . ' AUTO_INCREMENT = 0;');
}
